import React from 'react';
import top1 from '../assets/top1.png'
import Paper from '@material-ui/core/Paper';
import clsx from 'clsx';
import Footer from '../components/Footer';
import {
  Button, IconButton, DialogTitle,
  DialogContent,
  FormControl, Grid, InputLabel, MenuItem, Select,Divider,List, TextField, Typography, Card, CardContent, Accordion
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },

  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    marginLeft:"300px"
  },
  fixedHeight: {
    height: 'auto',
  },
}));


function Products() {
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  return (
    <div style={{ backgroundColor: "#e0e0e0" }} >
      <br/>
                                    {/* <Accordion defaultExpanded style={{ background: "#e0e0e0", marginTop: '10px' }}> */}
        <Grid container spacing={3}>

        <Grid style={{ padding: "20px" }} item xs={4}>
                <Paper elevation={3}>
            
                   
                    <img src={top1}   width="100%" />
                   
                </Paper>
            </Grid>
            
            <Grid style={{ padding: "20px" }} item xs={8}>
                <Paper elevation={3}>
                    <List component="nav" aria-label="mailbox folders">
                    <p>L’entrepreneuriat c’est quoi ? L’entrepreneuriat désigne l’action d’entreprendre, de mener à bien un projet. 

                          Souvent utilisé dans le secteur des affaires, le terme entreprendre signifie créer une activité (économique) pour atteindre un objectif, répondre à un besoin. Le créateur représente l’entrepreneur, soit le porteur du projet.

                    L’objet de l’entrepreneuriat correspond donc à votre projet</p>
                    <p>L’entrepreneuriat c’est quoi ? L’entrepreneuriat désigne l’action d’entreprendre, de mener à bien un projet. 

                      Souvent utilisé dans le secteur des affaires, le terme entreprendre signifie créer une activité (économique) pour atteindre un objectif, répondre à un besoin. Le créateur représente l’entrepreneur, soit le porteur du projet.

                      L’objet de l’entrepreneuriat correspond donc à votre projet</p>
                      <p>L’entrepreneuriat c’est quoi ? L’entrepreneuriat désigne l’action d’entreprendre, de mener à bien un projet. 

                      Souvent utilisé dans le secteur des affaires, le terme entreprendre signifie créer une activité (économique) pour atteindre un objectif, répondre à un besoin. Le créateur représente l’entrepreneur, soit le porteur du projet.

                      L’objet de l’entrepreneuriat correspond donc à votre projet</p>
                    </List>
                </Paper>
            </Grid>
         
            
            

            <Grid style={{ padding: "20px" }} item xs={4}>
               
            </Grid>
            <Grid style={{ padding: "20px" }} item xs={4}>
                <Paper elevation={3}>
                    <List component="nav" aria-label="mailbox folders">
                    <p style={{textAlign: "center"}}>Les infos a mettre</p>
                    </List>
                </Paper>
            </Grid>
            <Grid style={{ padding: "20px" }} item xs={4}>
                <Paper elevation={3}>
                    <List component="nav" aria-label="mailbox folders">
                    <p style={{textAlign: "center"}}>Les infos a mettre</p>
                    </List>
                </Paper>
            </Grid>


           



        </Grid>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <Footer/>


        {/* </Accordion> */}
                                  

   
    </div>
  );
}

export default Products;
