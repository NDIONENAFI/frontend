import logo from './logo.svg';
import './App.css';
import ListeEntrepreneur from './component/ListeEntrepreneur';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Reports from './pages/Reports';
import Products from './pages/Products';
import Main from './components/Main';
function App() {
  
  return (
    < >
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Main} />
          <Route path='/reports' component={ListeEntrepreneur} />
          <Route path='/products' component={Products} />
        </Switch>
      </Router>
  

      
    </>
  );
}

export default App;
